# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Assignment #2 Requirements:


![A2 Instructions Screenshot](img/part1.png)  
![A2 Instructions 2 Screenshot](img/part1_2.png)  

#### Assignment Screenshots:

*Create Company Table*:

![Company](img/create_company_table.png)

*Crete Customer Table*:

![JDK Installation Screenshot](img/create_customer_table.png)

*User 3 Login*:

![User 3](img/user_3_login.png)

*User 4 Login*:
![User 4](img/user_4_login.png)

*Admin Login*:
![Admin Login](img/admin_login.png)

*Current Version*:
![Current Version](img/admin_login.png)

*Company data*:
![Company](img/company_data.png)

*Customer data*:
![Customer](img/customer_data.png)

*Foreign Key 1*:
![Foreign Key 1](img/verify_fk_options_8.png)

*Foreign Key 2*:
![Foreign Key 2](img/verify_fk_options_9.png)

*User 3 Permissions*:
![User 3 Permissions](img/user3_denied_insert_customer.png)

*User 3 Permissions 2*:
![User 3 Permissions 2](img/user3_denied_insert_company.png)

*User 4 Permissions*:
![User 4 Permissions](img/user_4_denied_viewing_company.png)

*User 4 Permissions 2*:
![User 4 Permissions 2](img/user_4_denied_drop.png)

*Drop Tables as Admin*:
![Drop Tables](img/drop_tables_as_admin.png)
