use mas18u;



create user 'user3'@'localhost' identified by '1234';
create user 'user4'@'localhost' identified by '1234';

grant select, update, delete
on mas18u.*
to 'user3'@'localhost';

grant select, insert
on mas18u.customer
to 'user4'@'localhost';

show grants for 'user3'@'localhost';
show grants for 'user4'@'localhost';

select user from mysql.user;

use user3;
select current_user;

show tables;

select * from customer;

insert into company (cmp_id)
values (7);

drop table company, customer;

