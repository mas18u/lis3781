use mas18u;

create database assignment2;

create table company (
cmp_id INT unsigned not null auto_increment,
cmp_type ENUM ('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership') NOT NULL,
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip INT(9) unsigned NOT NULL,
cmp_phone bigint unsigned NOT NULL,
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL,
cmp_url VARCHAR(100) NOT NULL,
cmp_notes VARCHAR(255),
primary key (cmp_id)
)
engine = InnoDB character set utf8 collate utf8_general_ci;


drop table company;
drop table customer;

create table customer(
	cus_id INT unsigned not null auto_increment,
    cmp_id int unsigned not null,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null,
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first varchar(15) not null,
    cus_last varchar(30) not null,
    cus_street varchar(30) null,
    cus_city varchar(30) null,
    cus_state char(2) null,
    cus_zip int(9) unsigned zerofill null,
    cus_phone bigint unsigned not null,
    cus_email varchar(100) null,
    cus_balance decimal(7,2) unsigned null,
    cus_tot_sales decimal(7,2) unsigned null,
    cus_notes varchar(255) null,
    primary key (cus_id),
    
    unique index ux_cus_ssn(cus_ssn asc),
    index idx_cmp_id(cmp_id asc),
    
    constraint fk_customer_company
		foreign key (cmp_id)
        references company (cmp_id)
        on delete no action
        on update cascade
	)
    engine = innodb character set utf8 collate utf8_general_ci;
    
    show tables;
    
drop table company;

describe company;
describe customer;

set @salt=random_bytes(64);

insert into company 
values
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes');

insert into customer
values
(null, 1, unhex(sha2(concat(@salt, 001456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '123456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 2, unhex(sha2(concat(@salt, 002456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '133456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 3, unhex(sha2(concat(@salt, 003456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '124456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 4, unhex(sha2(concat(@salt, 004456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '125456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 5, unhex(sha2(concat(@salt, 005456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '126456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment');
 
 select *
 from customer;
 




