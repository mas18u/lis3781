use mas18u;

create table company (
cmp_id INT unsigned not null auto_increment,
cmp_type ENUM ('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership') NOT NULL,
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip INT(9) unsigned zerofill NOT NULL comment 'no dashes',
cmp_phone bigint unsigned NOT NULL comment 'ssn and zip codes can be zero filled, but not US area codes',
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL comment '12,325,678.90',
cmp_url VARCHAR(100) NOT NULL,
cmp_notes VARCHAR(255),
primary key (cmp_id)
)
engine = InnoDB character set utf8 collate utf8_general_ci;

drop table company;

describe company;
describe customer;


create table customer(
	cus_id INT unsigned not null auto_increment,
    cmp_id int unsigned not null,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null comment 'only for demo - do not use salt in the name',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first varchar(15) not null,
    cus_last varchar(30) not null,
    cus_street varchar(30) null,
    cus_city varchar(30) null,
    cus_state char(2) null,
    cus_zip int(9) unsigned zerofill null,
    cus_phone bigint unsigned not null comment 'ssn and zip codes can be zero filled, but not us area codes',
    cus_email varchar(100) null,
    cus_balance decimal(7,2) unsigned null comment '12,345.67',
    cus_tot_sales decimal(7,2) unsigned null,
    cus_notes varchar(255) null,
    primary key (cus_id),
    
    unique index ux_cus_ssn(cus_ssn asc),
    index idx_cmp_id(cmp_id asc),
    
    constraint fk_customer_company
		foreign key (cmp_id)
        references company (cmp_id)
        on delete no action
        on update cascade
	)
    engine = innodb character set utf8 collate utf8_general_ci;
    
    
create user 'user3'@'localhost' identified by '1234';
create user 'user4'@'localhost' identified by '1234';

grant select, update, delete
on local.*
to 'user3'@'localhost';

grant select, insert
on local.*
to 'user4'@'localhost';


show grants for 'user3'@'localhost';
show grants for 'user4'@'localhost';










