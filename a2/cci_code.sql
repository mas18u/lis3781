use mas18u;

show tables;

select * from company;
select * from customer;
set @salt=random_bytes(64);


insert into company 
values
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes'),
(null, 'C-Corp', '1 West', 'New York', 'NY', '123456789', '1112223333', '10000000.10', 'placeholder.com', 'notes');

insert into customer
values
(null, 1, unhex(sha2(concat(@salt, 001456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '123456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 2, unhex(sha2(concat(@salt, 002456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '133456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 3, unhex(sha2(concat(@salt, 003456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '124456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 4, unhex(sha2(concat(@salt, 004456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '125456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment'),
 (null, 5, unhex(sha2(concat(@salt, 005456789),512)), @salt, 'Discount', 'John', 'Smith', '1 West',
 'New York', 'NY', '126456789', '1112223333', 'customer@email.com', '10000.00', '10000.00', 'comment');