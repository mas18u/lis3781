# LIS 3781 Advanced Database Management

## Manolo Sanchez

### LIS 3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Screenshot of AMPPS
	- Create Bitbucket Repo
	- Complete Bitbucket tutorial
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create and populate company and customer tables
	- Create foreign keys
	- Create User 3 and User 4
	- Limit permissions of users
	- Test permissions of each user

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create and populate tables using Oracle SQL
	- Select data from tables using various criteria and formatting
	- Screenshot of Oracle SQL Code
	- Screenshot of populated tables
	- Screenshot of *optional* required reports

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create a database to keep track of court case data
	- Make sure the several business rules are met
5. [A4 README.md](a4/README.md "My A4 README.md file")  
	- Create a database for a home office supply company  
	- Database tracks history of products  
	- Screenshot of ERD  
	- Required reports SQL code and results  
	
6. [A5 README.md](a5/README.md "My A5 README.md file")  
	- Expand database for a home office supply company  
	- Implement a data mart  
	- Add warehousing analytics including time and location of product sold  
	- Required reports SQL code and results  

7. [P2 README.md](p2/README.md "My P2 README.md file")  
	- Set up mongoDB  
	- Import restaurants data into restaurants collection in the test database  
	- Show MongoDB shell commands  
	- Show JSON code for required reports  

