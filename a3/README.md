# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Assignment #3 Requirements:

*3 Parts*

1. Screenshot of Oracle SQL Code
2. Screenshot of populated tables
3. Screenshot of *optional* required reports


#### Assignment Screenshots:

*Creating Tables*:

![SQL Code](img/create_customer.png)

*Inserting Data*:

![Insert Data](img/insert_data.png)

*Populated Customer Table*:

![Customer](img/cus_table.png)

*Populated Commodity Table*:

![Commodity](img/com_table.png)

*Populated Order Table*:

![Order](img/ord_table.png)

*Required Reports*:

![Req Reports](img/required_reports1.png)

*Required Reports (cont)*:

![Req Reports2](img/required_reports2.png)