-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mas18u
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mas18u` ;

-- -----------------------------------------------------
-- Schema mas18u
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mas18u` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `mas18u` ;

-- -----------------------------------------------------
-- Table `mas18u`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mas18u`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT(5) UNSIGNED ZEROFILL NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`emp_ssn` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `mas18u`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mas18u`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`dependent` (
  `dep_id` SMALLINT UNSIGNED NOT NULL,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT(9) UNSIGNED NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT(9) UNSIGNED NOT NULL,
  `dep_phone` BIGINT UNSIGNED NOT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  UNIQUE INDEX `dep_ssn_UNIQUE` (`dep_ssn` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `mas18u`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mas18u`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`benefit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mas18u`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('single', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `mas18u`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `mas18u`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mas18u`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mas18u`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mas18u`.`emp_hist` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL,
  `eht_job_id` TINYINT NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) NOT NULL,
  `eht_usr_changed` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `mas18u`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mas18u`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (1, 'Worker1', NULL);
INSERT INTO `mas18u`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (2, 'Worker2', NULL);
INSERT INTO `mas18u`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (3, 'Worker3', NULL);
INSERT INTO `mas18u`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (4, 'Worker4', NULL);
INSERT INTO `mas18u`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (5, 'Boss', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mas18u`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (1, 1, 111111111, 'Bob', 'Smith', '2000-01-01', '2000-01-01', NULL, 50000, 'West', 'Tallahassee', 'FL', 32304, 1111111111, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (2, 2, 222222222, 'John', 'Smith', '2000-01-01', '2000-01-01', NULL, 50000, 'West', 'Tallahassee', 'FL', 32304, 1111111112, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (3, 3, 333333333, 'Mary', 'Brown', '2000-01-01', '2000-01-01', NULL, 50000, 'West', 'Tallahassee', 'FL', 32304, 1111111113, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (4, 4, 444444444, 'Kary', 'Brown', '2000-01-01', '2000-01-01', NULL, 50000, 'West', 'Tallahassee', 'FL', 32304, 1111111114, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (5, 5, 555555555, 'Larry', 'David', '2000-01-01', '2000-01-01', NULL, 100000, 'West', 'Tallahassee', 'FL', 32304, 1111111115, '123@yahoo.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mas18u`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (1, 1, '2000-01-01', 111111111, 'Jane', 'Smith', '2005-01-01', 'Child', 'West', 'Tallahassee', 'FL', 32304, 1111111111, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (2, 2, '2000-01-01', 222222222, 'Jake', 'Smith', '2005-01-01', 'Child', 'West', 'Tallahassee', 'FL', 32304, 1111111112, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (3, 3, '2000-01-01', 333333333, 'Kate', 'Brown', '2005-01-01', 'Child', 'West', 'Tallahassee', 'FL', 32304, 1111111113, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (4, 4, '2000-01-01', 444444444, 'Jerry', 'Brown', '2005-01-01', 'Child', 'West', 'Tallahassee', 'FL', 32304, 1111111114, '123@yahoo.com', NULL);
INSERT INTO `mas18u`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (5, 5, '2000-01-01', 555555555, 'Cheryl', 'David', '2005-01-01', 'Spouse', 'West', 'Tallahassee', 'FL', 32304, 1111111115, '123@yahoo.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mas18u`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (1, 'health', NULL);
INSERT INTO `mas18u`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (2, 'health2', NULL);
INSERT INTO `mas18u`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (3, 'health3', NULL);
INSERT INTO `mas18u`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (4, 'health4', NULL);
INSERT INTO `mas18u`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (5, 'health5', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mas18u`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (1, 1, 1, 'family', 5000.00, '2000-01-01', NULL);
INSERT INTO `mas18u`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (2, 2, 1, 'family', 5000.00, '2000-01-01', NULL);
INSERT INTO `mas18u`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (3, 3, 1, 'family', 5000.00, '2000-01-01', NULL);
INSERT INTO `mas18u`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (4, 4, 1, 'family', 5000.00, '2000-01-01', NULL);
INSERT INTO `mas18u`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (5, 5, 1, 'family', 5000.00, '2000-01-01', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mas18u`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `mas18u`;
INSERT INTO `mas18u`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (1, 1, '2000-01-01 12:00:59', 'i', 1, 50000, 'none', 'none', NULL);
INSERT INTO `mas18u`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (2, 2, '2000-01-01 12:00:59', 'i', 2, 50000, 'none', 'none', NULL);
INSERT INTO `mas18u`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (3, 3, '2000-01-01 12:00:59', 'i', 3, 50000, 'none', 'none', NULL);
INSERT INTO `mas18u`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (4, 4, '2000-01-01 12:00:59', 'i', 4, 50000, 'none', 'none', NULL);
INSERT INTO `mas18u`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (5, 5, '2000-01-01 12:00:59', 'i', 5, 100000, 'none', 'none', NULL);

COMMIT;

