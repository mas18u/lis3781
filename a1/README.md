# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Assignment 1 Requirements:

*Five Parts*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS installation
3. Questions
4. Entity Relationship Disagram, and SQL Code (optional)
5. Bitbucket repo links:  
	a) this assignment  
	b) completed tutorial
### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database
modeler/designer to collect the following employee data for tax purposes: job description, length of
employment, benefits, number of dependents and their relationships, DOB of both the employee and any
respective dependents. In addition, employees’ histories must be tracked. Also, include the following
business rules:  
• Each employee may have one or more dependents.  
• Each employee has only one job.  
• Each job can be held by many employees.  
• Many employees may receive many benefits.  
• Many benefits may be selected by many employees (though, while they may not select any benefits—
any dependents of employees may be on an employee’s plan).  
  
Notes:  
• Employee/Dependent tables must use suitable attributes (See Assignment Guidelines);  
In Addition:  
• Employee: SSN, DOB, start/end dates, salary;  
• Dependent: same information as their associated employee (though, not start/end dates), date added
(as dependent), type of relationship: e.g., father, mother, etc.  
• Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)  
• Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)  
• Plan: type (single, spouse, family), cost, election date (plans must be unique)  
• Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;  
• Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);  
• *All* tables must include notes attribute.  
Design Considerations:   
Generally, avoid using flag values (e.g., yes/no) for status—unless, applicable.
Instead, use dates when appropriate, as date values provide more information, and sometimes, can be
used when a flag value would be used. For example, “null” values in employees’ termination dates would
indicate “active” employees.  


#### README.md file should include the following items:

* Screenshot of A1 ERD
* Query and result screenshots
* git commands w/short descriptions

*Git commands w/short descriptions:*
1. git init - creates an empty git repository  
2. git status - shows the working tree status  
3. git add - adds file contents to the index  
4. git commit - record changes to the repository  
5. git push - update remote refs along with associated objects  
6. git pull - fetch from and integrate with another repository or a local branch  
7. git clone - downloads existing source code from a remote repository


#### Assignment Screenshots:

*Screenshot of Ampps*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of A1 ERD*:

![A1 ERD](img/erd.png)

*Screenshot of Query 1*:
![Query 1] (img/q1.png)

*Screenshot of Query 2*:
![Query 2] (img/q2.png)

*Screenshot of Query 3*:
![Query 3] (img/q3.png)  

*Screenshot of Query 4 Before*:  
![Query 4 Before] (img/q4before.png)

*Screenshot of Query 4 After*:  
![Query 4 After] (img/q4after.png)

*Screenshot of Query 5 Before*:  
![Query 5 Before] (img/q5before.png)

*Screenshot of Query 5 After*:  
![Query 5 After] (img/q5after.png)

*Screenshot of Query 6*:  
![Query 6] (img/q6.png)

*Screenshot of Query 7*:  
![Query 7] (img/q7.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mas18u/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
