# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Assignment #4 Requirements:

1. Create a database for a home office supply company
2. Database tracks history of products
3. Screenshot of ERD
4. Required reports SQL code and results


#### Assignment Screenshots:

*ERD*:

![erd](img/erd.png)

*Report 1*:

![q1](img/q1.png)

*Report 2*:

![q2](img/q2.png)

*Report 3*:

![q3](img/q3.png)

*Report 4 Before Trigger*:

![q4_before](img/q4_before.png)

*Report 4 After Trigger*:*:

![q4_after](img/q4_after.png)

*Report 5 Before Trigger*:

![q5_before](img/q5_before.png)

*Report 5 After Trigger*:*:

![q5_after](img/q5_after.png)