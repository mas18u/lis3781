
# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Project 2 Requirements:

1. Set up mongoDB  
2. Import restaurants data into restaurants collection in the test database  
3. Show MongoDB shell commands  
4. Show JSON code for required reports  

#### Assignment Screenshots:

*MongoDB shell commands*:  
![ERD](img/shell.png)  

*R1*:  
![R1](img/r1.png)  

*R2*:  
![R2](img/r2.png)  

*R3*:  
![R3](img/r3.png)  

*R4*:  
![R4](img/r4.png)  