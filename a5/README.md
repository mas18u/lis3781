# LIS 3781 - Advanced Database Management

## Manolo Sanchez

### Assignment #5 Requirements:

1. Expand database for a home office supply company
2. Implement a data mart
3. Add warehousing analytics including time and location of product sold
4. Required reports SQL code and results


#### Assignment Screenshots:

*ERD*:

![erd](img/erd.png)

*Report 1*:

![r1](img/r1.png)

*Report 2*:

![r2](img/r2.png)

*Report 3 Before*:

![r3](img/r3_before.png)

*Report 3 After*:

![q4_before](img/r3_after.png)

*Report 4*:*:

![r4](img/r4.png)

*Report 5*:

![r5](img/r5.png)

*Report 6 Before Trigger*:*:

![r6before](img/r6_before.png)

*Report 6 After Trigger*:*:

![r6after](img/r6_after.png)