use mas18u;

select * from payment;

SELECT
  *
FROM
  SYSOBJECTS
WHERE
  xtype = 'U';

select * from phone;

 CREATE TABLE dbo.time
(
tim_id INT NOT NULL identity(1,1),
tim_yr SMALLINT NOT NULL,
tim_qtr TINYINT NOT NULL,
tim_month TINYINT NOT NULL,
tim_week TINYINT NOT NULL,
tim_day TINYINT NOT NULL, 
tim_time TIME NOT NULL,
tim_notes VARCHAR(255) NULL,
PRIMARY KEY (tim_id)
);

CREATE TABLE region
(
reg_id TINYINT NOT NULL Identity(1,1),
reg_name CHAR(1) NOT NULL,
reg_notes VARCHAR(255) NULL,
PRIMARY KEY (reg_id)
);

CREATE TABLE dbo.state
(
ste_id TINYINT NOT NULL identity(1,1),
reg_id TINYINT NOT NULL,
ste_name CHAR(2) NOT NULL DEFAULT 'FL',
ste_notes VARCHAR(255) NULL,
PRIMARY KEY (ste_id),

CONSTRAINT fk_state_region
FOREIGN KEY(reg_id)
REFERENCES dbo.region(reg_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE dbo.city
(
cty_id SMALLINT NOT NULL identity(1,1),
ste_id TINYINT NOT NULL,
cty_name VARCHAR(30) NOT NULL,
cty_notes VARCHAR(255) NULL,
PRIMARY KEY (cty_id),

CONSTRAINT fk_city_state
FOREIGN KEY(ste_id)
REFERENCES dbo.state(ste_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;

IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;

IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;

select * from store;


CREATE TABLE dbo.store
(
str_id SMALLINT NOT NULL identity(1,1),
cty_id SMALLINT NOT NULL,
str_name VARCHAR(45) NOT NULL,
str_street VARCHAR(30) NOT NULL,
str_zip Int NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_phone bigint NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_email VARCHAR(100) NOT NULL,
str_url VARCHAR(100) NOT NULL,
str_notes VARCHAR(255) NULL,
PRIMARY KEY (str_id),

CONSTRAINT fk_store_city
FOREIGN KEY(cty_id)
REFERENCES dbo.city(cty_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE dbo.sale
(
pro_id SMALLINT NOT NULL,
str_id SMALLINT NOT NULL,
cnt_id INT NOT NULL,
tim_id INT NOT NULL,
sal_qty SMALLINT NOT NULL,
sal_price DECIMAL(8,2) NOT NULL,
sal_total DECIMAL(8,2) NOT NULL,
sal_notes VARCHAR(255) NULL,
PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),
CONSTRAINT fk_sale_time
FOREIGN KEY (tim_id)
REFERENCES dbo.time(tim_id)
ON DELETE CASCADE
ON UPDATE CASCADE,

CONSTRAINT fk_sale_contact
FOREIGN KEY (cnt_id)
REFERENCES dbo.contact(cnt_id)
ON DELETE CASCADE
ON UPDATE CASCADE,

CONSTRAINT fk_sale_store
FOREIGN KEY (str_id)
REFERENCES dbo.store(str_id)
ON DELETE CASCADE
ON UPDATE CASCADE,

CONSTRAINT fk_sale_product
FOREIGN KEY (pro_id)
REFERENCES dbo.product(pro_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

INSERT INTO region
(reg_name,  reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);

INSERT INTO state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'IL', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL);

INSERT INTO city
(ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(3, 'Chicago', NULL),
(4, 'Clover', NULL),
(5, 'St. Louis', NULL);

select * from city;

INSERT INTO dbo.store
(cty_id, str_name, str_street, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2, 'Walgreens', '123 Main Street', 111111111, 1112223333, 'info@walgreens.com', 'walgreens.com', NULL),
(3, 'CVS', '123 Apple Street', 222222222, 2223334444, 'info@CVS.com', 'CVS.com', NULL),
(4, 'Walmart', '123 Acorn Street', 333333333, 3334445555, 'info@Walmart.com', 'Walmart.com', NULL),
(5, 'Publix', '123 West Street', 444444444, 4445556666, 'info@Publix.com', 'Publix.com', NULL),
(1, 'Home Depot', 'East Main Street', 555555555, 5556667777, 'info@HomeDepot.com', 'HomeDepot.com', 'Recently sold property');

INSERT INTO time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL);


select * from product;
select * from contact;
select * from region;
select * from store;

INSERT INTO sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(5, 1, 1, 1, 10, 24.99, 249.90, NULL),
(5, 1, 1, 2, 10, 24.99, 249.90, NULL),
(5, 1, 1, 3, 10, 24.99, 249.90, NULL),
(6, 2, 2, 4, 10, 15.99, 159.90, NULL),
(6, 2, 2, 5, 10, 15.99, 159.90, NULL),
(6, 2, 2, 6, 10, 15.99, 159.90, NULL),
(2, 3, 3, 7, 10, 8.99, 89.90, NULL),
(2, 3, 3, 8, 10, 8.99, 89.90, NULL),
(2, 3, 3, 9, 10, 8.99, 89.90, NULL),
(3, 4, 4, 10, 10, 4.99, 49.90, NULL),
(3, 4, 4, 11, 10, 4.99, 49.90, NULL),
(3, 4, 4, 12, 10, 4.99, 49.90, NULL),
(1, 5, 5, 13, 10, 11.99, 111.90, NULL),
(1, 5, 5, 14, 10, 11.99, 111.90, NULL),
(1, 5, 5, 15, 10, 11.99, 111.90, NULL),
(6, 2, 2, 16, 10, 15.99, 159.90, NULL),
(2, 3, 3, 17, 10, 8.99, 89.90, NULL),
(2, 3, 3, 18, 10, 8.99, 89.90, NULL),
(2, 3, 3, 19, 10, 8.99, 89.90, NULL),
(3, 4, 4, 20, 10, 4.99, 49.90, NULL),
(3, 4, 4, 21, 10, 4.99, 49.90, NULL),
(3, 4, 4, 22, 10, 4.99, 49.90, NULL),
(1, 5, 5, 23, 10, 11.99, 111.90, NULL),
(1, 5, 5, 24, 10, 11.99, 111.90, NULL),
(1, 5, 5, 25, 10, 11.99, 111.90, NULL);


GO
CREATE PROC dbo.product_days_of_week AS
BEGIN
select pro_name, pro_descript, datename(dw,tim_day-1)
from product p
join sale s on p.pro_id=s.pro_id
join time t on t.tim_id=s.tim_id
order by tim_day-1 asc;
END
GO

exec dbo.product_days_of_week;


GO
CREATE PROC dbo.product_dril_down AS
BEGIN
select pro_name, pro_goh, str_name, cty_name, ste_name, reg_name
from product p
join sale s on p.pro_id=s.pro_id
join store sr on sr.str_id=s.str_id
join city c on sr.cty_id=c.cty_id
join state st on c.ste_id=st.ste_id
join region r on st.reg_id=r.reg_id
order by pro_goh desc;
END
GO

select * from invoice;

exec dbo.product_dril_down;

drop proc dbo.add_payment;

GO
CREATE PROC dbo.add_payment
@inv_id_p int,
@pay_date_p datetime,
@pay_amt_p decimal(7,2),
@pay_notes_p varchar(255)
AS
BEGIN
INSERT INTO payment(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(@inv_id_p, @pay_date_p, @pay_amt_p, @pay_notes_p);
END
GO

DECLARE
@inv_id_v int = 6,
@pay_date_v DATETIME = '2014-01-05 11:56:38',
@pay_amt_v DECIMAL(7,2) = 159.99,
@pay_notes_V VARCHAR(255) = 'testing add_payment';

exec dbo.add_payment @inv_id_v, @pay_date_v, @pay_amt_v, @pay_notes_v;

select * from invoice;

GO
CREATE PROC dbo.customer_balance
@per_lname_p varchar(30)
AS
BEGIN
select p.per_id, per_fname, per_lname, i.inv_id,
FORMAT(sum(pay_amt), 'C', 'en-us') as total_paid,
FORMAT((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
from person p
join dbo.customer c on p.per_id=c.per_id
join dbo.contact ct on c.per_id=ct.per_cid
join dbo.[order] o on ct.cnt_id=o.cnt_id
join dbo.invoice i on o.ord_id=i.ord_id
join dbo.payment pt on i.inv_id=pt.inv_id

where per_lname=@per_lname_p
group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
END
GO

DECLARE @per_lname_v varchar(30) = 'Smith';
exec dbo.customer_balance @per_lname_v;

GO
CREATE PROC store_sales_between_dates
@start_date_p smallint,
@end_date_p smallint

AS
BEGIN
	select st.str_id, FORMAT(SUM(sal_total), 'C', 'en-us') as 'total sales', tim_yr as year
	from store st
	join sale s on st.str_id=s.str_id
	join time t on s.tim_id=t.tim_id
	where tim_yr between @start_date_p and @end_date_p
	group by tim_yr, st.str_id
	order by sum(sal_total) desc, tim_yr desc;
END
GO

DECLARE
@start_date_v smallint = 2010,
@end_date_v smallint = 2013;

exec dbo.store_sales_between_dates @start_date_v, @end_date_v;



GO
CREATE TRIGGER dbo.trg_check_inv_paid
ON dbo.payment
AFTER INSERT AS
BEGIN

update invoice
set inv_paid=0;

UPDATE invoice
SET inv_paid=1
FROM invoice as i
join
(
	SELECT inv_id, sum(pay_amt) as total_paid
	FROM payment
	GROUP BY inv_id
) as v ON i.inv_id=v.inv_id
WHERE total_paid >= inv_total;

END
GO

select * from invoice;
select * from payment;


INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(3, '2014-07-04', 75.00, 'Paid by check');




